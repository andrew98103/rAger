var rager_now = Math.floor((new Date).getTime()/1000)
var newReddit = document.querySelector('#SHORTCUT_FOCUSABLE_DIV') !== null;
const allAuthors = {}
const allAuthorsByName = {}

const extensionClass = "rager-info"
let extensionClasses = extensionClass
if (newReddit) {
  extensionClasses += " rager-redesign"
}


const process_oldReddit_items = async (items) => {
    let author_ids = $(items).map(function () {
        return $(this).attr("data-author-fullname");
    }).get();

    await lookupAuthorData_and_apply(author_ids)
    for (let t of $('.tagline time')) {
        let itemDate = Math.floor(new Date(t.getAttribute('title')).getTime()/1000)
        let diff = rager_now-itemDate
        if (diff > 86400) {
            let pretty_age = convertSecondsToReadableTime(diff) + ' ago';
            //future work: make option to display full date with selectable time zone
            //let pretty_age = (new Date(itemDate*1000)).toLocaleString("en-US", {timeZone: "America/New_York"}) + ' ET'
            let clone = $(t).clone()
            $(clone).attr('class','').html(pretty_age);
            $(t).after(clone);
            $(t).remove();
        }
    }
}

function process_newReddit() {
    window.fetch(document.URL)
        .then(data => data.text())
        .then(html => {
            var matcher = html.match(/window\.___r = ([\s\S]*);</);
            if (matcher && matcher.length > 1) {
                json = JSON.parse(matcher[1]);
                var comments = json.features.comments.models;
                var posts = json.posts.models;
                var items = Object.assign({}, comments, posts)
                lookupAuthorData_and_process_newReddit_items(items)
            }
        })
}

const lookupAuthorDataForItems = async (items) => {
  author_ids = Object.keys(items).map(k => items[k].authorId)
  await getAuthorData(author_ids)
}

const lookupAuthorData_and_process_newReddit_items = async (items) => {
  await lookupAuthorDataForItems(items)
  process_newReddit_items(items)
}

const process_newReddit_items = async (items) => {
    applyAuthorData(allAuthors)
    $.each(items, function(id, item) {
        let created = item.created
        if (created > rager_now) {
            created = created / 1000;
        }
        let pretty_time = convertSecondsToReadableTime(rager_now-created)+' ago';
        $(`a[id*="${id}"]`).html(pretty_time);
        $(`a[id$="${id}inOverlay]"`).html(pretty_time);
        if (item.id.substr(0,2) === 't3' && item.id.length < 20) {
          $('#'+item.id).find('[data-click-id="timestamp"], [data-testid="comment_timestamp"], [data-testid="post_timestamp"]').html(pretty_time);
        }
    })
}

// wrote this to process newly arrived comments that were not processed by
// code that runs via monitor-requests, but it ends up being cleaner
// to just delay processing in monitor-requests than to try to catch these as they render.
// was previously using arrive.js to track arrival of .Comment in new-reddit.js
// keeping this function for now in case needed later
const process_newReddit_cached_comments = async (items) => {
  const setAuthors = new Set()
  for (const item of items) {
    authorLink = $(item).find('a[data-testid="comment_author_link"]')[0]
    // removed comments have no author link
    if (authorLink) {
      const authorName = authorLink.href.replace(/\/*$/,'').split('/').slice(-1)[0]
      setAuthors.add(authorName)
      const authorObj = allAuthorsByName[authorName]
      if (authorObj) {
        const {prettyAge, karma} = getPrettyAgeAndKarma(authorObj)
        insertAuthorMeta(authorLink, authorName, prettyAge, karma)
      }
    }
  }
}

function insertAuthorMeta(authorLink, authorName, prettyAge, karma) {
  if ( (!$(authorLink).next() || ! $(authorLink).next().hasClass(extensionClass)) &&
       $(authorLink).text().match(authorName)) {
    $(authorLink).after(`<span class="${extensionClasses}">[${prettyAge} | ${karma}]</span>`);
  }
}

function getPrettyAgeAndKarma(author) {
  const prettyAge = convertSecondsToReadableTime(rager_now-author.created_utc);
  const karma = addCommas(author.link_karma + author.comment_karma);
  return {prettyAge, karma}
}

const lookupAuthorData_and_apply = (ids) => {
  return getAuthorData(ids).then(applyAuthorData)
}

function applyAuthorData(authorDict = allAuthors) {
  $.each(authorDict, function(id, author) {
    const {prettyAge, karma} = getPrettyAgeAndKarma(author)
    $(`a[href$="/user/${author.name}"], a[href$="/user/${author.name}/"]`).each(function(idx, element) {
      insertAuthorMeta(element, author.name, prettyAge, karma)
    })
  });
}

const getAuthorData = async (ids_with_duplicates) => {
    if (ids_with_duplicates && ids_with_duplicates.length) {
        const ids_set = new Set()
        for (const id of ids_with_duplicates) {
          if (! allAuthors[id]) {
            ids_set.add(id)
          }
        }
        const ids = Array.from(ids_set)
        return getAuth()
        .then(async (auth) => {
            for (const [i, ids_chunk] of chunk(ids, 500).entries()) {
                // if (i !== 0) {
                //     await new Promise(r => setTimeout(r, 2000))
                // }
                const result = await window.fetch(`https://oauth.reddit.com/api/user_data_by_account_ids.json?ids=${ids_chunk.join(',')}`, auth)
                  .then(data => data.json())
                for (const [id, obj] of Object.entries(result)) {
                  allAuthors[id] = obj
                  allAuthorsByName[obj.name] = obj
                }
            }
            return allAuthors
        })
    }
    return allAuthors
}

// Split array into chunks of given size
const chunk = (arr, size) => {
  const chunks = []
  for (let i = 0; i < arr.length; i += size) {
    chunks.push(arr.slice(i, i + size))
  }
  return chunks
}

// Flatten arrays to one level
const flatten = arr => arr.reduce(
  (accumulator, value) => accumulator.concat(value),
  []
)


function convertSecondsToReadableTime(seconds) {
    var thresholds = [[60, 'second', 'seconds'], [60, 'minute', 'minutes'], [24, 'hour', 'hours'], [7, 'day', 'days'],
                     [365/12/7, 'week', 'weeks'], [12, 'month', 'months'], [10, 'year', 'years'],
                     [10, 'decade', 'decades'], [10, 'century', 'centuries'], [10, 'millenium', 'millenia']];
    var time = seconds;

    for (var i=0; i<thresholds.length; i++) {
        var divisor = thresholds[i][0];
        var text = thresholds[i][1];
        var textPlural = thresholds[i][2];
        if (time < divisor) {
            var extra = (time - Math.floor(time));
            var prevUnitTime = Math.round(extra*thresholds[i-1][0]);
            if (Math.floor(time) > 1 || Math.floor(time) == 0) {
                text = textPlural;
            }
            if (i > 1 && prevUnitTime > 0) {
                var remainText = thresholds[i-1][1];
                if (prevUnitTime > 1) {
                    remainText = thresholds[i-1][2];
                }
                text += ', ' + String(prevUnitTime) + ' ' + remainText;
            }
            return String(Math.floor(time)) + ' ' + text;
        }
        time = time / divisor;
    }
}

function addCommas (x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
