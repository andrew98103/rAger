monkeyPatch_XMLHttpRequest()

// putting the below here instead of src/content.js means it's in same process as the monkey patch.
// that allows allAuthors to function as a global variable


process_newReddit(); // processes content that appears in HTML before javascript runs

// Below not needed b/c repeat delayed processing in monitor-requests works better

// process content that:
//   (a) arrives via javascript, and
//   (b) does not trigger a request to Reddit b/c Reddit caches the results

// let newItems = []
// let calledGatherFunc = false
// $(document).arrive(".Comment", function() {
//     newItems.push(this);
//     setTimeout(function () {
//         if (! calledGatherFunc) {
//             calledGatherFunc = true;
//             console.log('calling gather func')
//             process_newReddit_cached_comments(newItems);
//             setTimeout(function () {
//                 calledGatherFunc = false;
//                 newItems = [];
//             }, 500);
//         }
//     }, 1000);
// });
